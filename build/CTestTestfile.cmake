# CMake generated Testfile for 
# Source directory: /home/meng/my_projects/quadrator_replanning_ws/src
# Build directory: /home/meng/my_projects/quadrator_replanning_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("ewok/catkin_simple")
subdirs("ewok/ewok_poly_spline")
subdirs("ewok/forest_gen")
subdirs("ewok/mav_comm/mav_comm")
subdirs("ewok/rotors_simulator/rotors_description")
subdirs("ewok/rotors_simulator/rotors_simulator")
subdirs("ewok/rotors_simulator/rotors_comm")
subdirs("ewok/mav_comm/mav_msgs")
subdirs("ewok/rotors_simulator/rotors_evaluation")
subdirs("ewok/rotors_simulator/rqt_rotors")
subdirs("ewok/ewok_ring_buffer")
subdirs("ewok/ewok_optimization")
subdirs("ewok/mav_comm/mav_planning_msgs")
subdirs("ewok/rotors_simulator/rotors_control")
subdirs("ewok/rotors_simulator/rotors_hil_interface")
subdirs("ewok/rotors_simulator/rotors_joy_interface")
subdirs("ewok/ewok_simulation")
subdirs("ewok/rotors_simulator/rotors_gazebo_plugins")
subdirs("ewok/octomap_rviz_plugins")
subdirs("ewok/rotors_simulator/rotors_gazebo")

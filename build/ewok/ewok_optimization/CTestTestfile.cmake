# CMake generated Testfile for 
# Source directory: /home/meng/my_projects/quadrator_replanning_ws/src/ewok/ewok_optimization
# Build directory: /home/meng/my_projects/quadrator_replanning_ws/build/ewok/ewok_optimization
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_ewok_optimization_gtest_test_uniform_bspline_3d_optimization "/home/meng/my_projects/quadrator_replanning_ws/build/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/melodic/share/catkin/cmake/test/run_tests.py" "/home/meng/my_projects/quadrator_replanning_ws/build/test_results/ewok_optimization/gtest-test_uniform_bspline_3d_optimization.xml" "--return-code" "/home/meng/my_projects/quadrator_replanning_ws/devel/lib/ewok_optimization/test_uniform_bspline_3d_optimization --gtest_output=xml:/home/meng/my_projects/quadrator_replanning_ws/build/test_results/ewok_optimization/gtest-test_uniform_bspline_3d_optimization.xml")
